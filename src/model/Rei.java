package model;

import java.awt.Point;
import java.util.ArrayList;

public class Rei extends Piece {

	public Rei() {
		
	}

	public Rei(String imagePath) {
		super(imagePath);
		
	}
	
	@Override
	public ArrayList<Point> getPosition(int row, int col) {
		zerarPosition();
		for(int i = -1; i<=1; i++){
			if(i!=0){
					addNewPosition(row + i, col);
					addNewPosition(row, col + i);
					addNewPosition(row + i, col + i);
					addNewPosition(row - i, col + i);
			}
		}
		return super.getPosition(row, col);
	}

}
