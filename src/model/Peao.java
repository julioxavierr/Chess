package model;

import java.awt.Point;
import java.util.ArrayList;

public class Peao extends Piece {

	public Peao() {
		
	}

	public Peao(String imagePath) {
		super(imagePath);
		
	}
	
	@Override
	public ArrayList<Point> getPosition(int row, int col) {
		zerarPosition();
		
		if(this.getColor().equalsIgnoreCase("Yellow")){
			if(row == 1){
				addNewPosition(row + 1, col);
				addNewPosition(row + 2, col);
				addNewPosition(row + 1, col + 1);
				addNewPosition(row + 1, col - 1);
			}else{
				addNewPosition(row + 1, col + 1);
				addNewPosition(row + 1, col - 1);
				addNewPosition(row + 1, col);
			}
		}else if(this.getColor().equalsIgnoreCase("White")){
			if(row == 6){
				addNewPosition(row - 1, col);
				addNewPosition(row - 2, col);
				addNewPosition(row - 1, col - 1);
				addNewPosition(row - 1, col + 1);
			}else{
				addNewPosition(row - 1, col - 1);
				addNewPosition(row - 1, col + 1);
				addNewPosition(row - 1, col);
			}
		}
		
		return super.getPosition(row, col);
	}

}
